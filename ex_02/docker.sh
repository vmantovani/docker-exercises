#!/bin/bash

sudo docker volume create data-nginx 
sudo docker run -it --rm -d -p 8080:80 --name container-1 --mount source=data-nginx,destination=/shared-volume nginx:latest &&  
sudo docker run -it --rm -d -p 3333:33 --name container-2 --mount source=data-nginx,destination=/shared-volume nginx:latest


# linha 3 cria um voluime compartilhado com o nome de data-nginx
# linhha 4 e 5 criar dois containers fazendo uso desse volume